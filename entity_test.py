import unittest
from entity import Entity


class TestEntity(unittest.TestCase):
    def test_new_default_values(self):
        e = Entity()
        self.assertEqual(e.x, 0)
        self.assertEqual(e.y, 0)
        self.assertEqual(e.state, Entity.ACTIVE)

    def test_new_with_position(self):
        e = Entity(x = 25, y = 40)
        self.assertEqual(e.x, 25)
        self.assertEqual(e.y, 40)

    def test_get_pos(self):
        e = Entity(x = 25, y = 40)
        self.assertEqual(e.pos, (25, 40))
        e.x = 30
        self.assertEqual(e.pos, (30, 40))
        e.y = 60
        self.assertEqual(e.pos, (30, 60))

    def test_set_pos(self):
        e = Entity()
        e.pos = (30, 60)
        self.assertEqual(e.pos, (30, 60))
        self.assertEqual(e.x, 30)
        self.assertEqual(e.y, 60)

    def test_get_size(self):
        e = Entity()
        e.width, e.height = (20, 25)
        self.assertEqual(e.width, 20)
        self.assertEqual(e.height, 25)
        self.assertEqual(e.size, (20, 25))
        e.width = 30
        self.assertEqual(e.size, (30, 25))
        e.height = 60
        self.assertEqual(e.size, (30, 60))

    def test_set_size(self):
        e = Entity()
        e.size = (30, 60)
        self.assertEqual(e.size, (30, 60))
        self.assertEqual(e.width, 30)
        self.assertEqual(e.height, 60)

    def test_get_hitbox_size(self):
        e = Entity()
        e.hitbox_width, e.hitbox_height = (20, 25)
        self.assertEqual(e.hitbox_width, 20)
        self.assertEqual(e.hitbox_height, 25)
        self.assertEqual(e.hitbox_size, (20, 25))
        e.hitbox_width = 30
        self.assertEqual(e.hitbox_size, (30, 25))
        e.hitbox_height = 60
        self.assertEqual(e.hitbox_size, (30, 60))

    def test_set_hitbox_size(self):
        e = Entity()
        e.hitbox_size = (30, 60)
        self.assertEqual(e.hitbox_size, (30, 60))
        self.assertEqual(e.hitbox_width, 30)
        self.assertEqual(e.hitbox_height, 60)


if __name__ == "__main__":
    unittest.main()
