from weakref import WeakValueDictionary
import os
import pygame
import bulletml


RES_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), "res"))


def load_image(string_id):
    """Load an image as a surface and memorize it."""
    if not resources.has_key(string_id):
        image = pygame.image.load(os.path.join(RES_PATH, string_id))
        resources[string_id] = image

    return resources[string_id]

def load_bulletml(string_id):
    """Load a BulletML document and memorize it."""
    if not bulletml_resources.has_key(string_id):
        bulletml_resources[string_id] = bulletml.BulletML.FromDocument(
                                open(os.path.join(RES_PATH, string_id), "rU"))

    return bulletml_resources[string_id]

def slice_animated_sprite(image, (w, h)):
    """
    Slice an image surface and return subsurfaces based on size
    parameter. Useful for animations.

    Image should contain animation frames placed horizontally.

    """
    width, height = image.get_size()
    images = []

    for i in xrange(int(width / w)):
        images.append(image.subsurface((i * w, 0, w, h)))

    return images


"""Initialize Resource Manager."""
if not pygame.image.get_extended():
    print("Warning: pygame cannot open extended image formats")

resources = WeakValueDictionary()
bulletml_resources = {}

