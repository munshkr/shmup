#!/usr/bin/env python
from event_manager import EventManager
import models
import views
import controllers
import cProfile
import pstats

try:
    import psyco
except ImportError:
    pass
else:
    psyco.full()


def main():
    em = EventManager()

    game = models.Game(event_manager = em)
    clock = controllers.Clock(max_fps = 40,
                              event_manager = em)

    pygame = views.Pygame(use_fullscreen = False,
                          viewport_size = game.VIEWPORT_SIZE,
                          resolution = (640, 480),
                          bitdepth = 32,
                          event_manager = em)
    kbd = controllers.Keyboard(event_manager = em)

    game.start()
    clock.run()


if __name__ == "__main__":
    main()
    #cProfile.run('main()', 'main')
    #p = pstats.Stats('main')
    #p.strip_dirs().sort_stats('cumulative').print_stats()
    #p.strip_dirs().sort_stats('time').print_stats()
