class Event(object):
    def __repr__(self):
        return '<%s %s>' % (self.__class__.__name__, id(self))


class Tick(Event):
    pass


class GameStart(Event):
    pass


class PlayerUnitSpawn(Event):
    def __init__(self, unit):
        self.unit = unit

class PlayerUnitHit(Event):
    def __init__(self, unit):
        self.unit = unit


class PlayerUnitMoveRequest(Event):
    def __init__(self, directions):
        self.directions = directions

    def __repr__(self):
        return '<%s %s: directions=%s>' % (self.__class__.__name__, id(self), self.directions)


class PlayerUnitMove(Event):
    def __init__(self, unit):
        self.unit = unit

    def __repr__(self):
        return '<%s %s: unit.pos=(%i, %i)>' % (self.__class__.__name__, id(self), self.unit.x, self.unit.y)


class PlayerUnitPrimaryFireRequest(Event):
    pass


class PlayerUnitPrimaryFire(Event):
    def __init__(self, bullets):
        self.bullets = bullets


class EnemyUnitSpawn(Event):
    def __init__(self, unit):
        self.unit = unit


class BalanceBarInit(Event):
    def __init__(self, hud_item):
        self.hud_item = hud_item


class EnemyUnitHit(Event):
    def __init__(self, unit):
        self.unit = unit


class EnemyUnitPrimaryFire(Event):
    def __init__(self, bullets):
        self.bullets = bullets


class EnemyBulletFire(Event):
    def __init__(self, bullets):
        self.bullets = bullets


class Quit(Event):
    pass
