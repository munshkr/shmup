import pygame
import resource_manager
import numpy as np

TRANSPARENT_COLOR = (136, 176, 120)
WHITE = (255, 255, 255)

class Sprite(pygame.sprite.Sprite):
    """
    Convenient wrapper around Pygame's Sprite class

    """
    def __init__(self, image, *groups):
        pygame.sprite.Sprite.__init__(self, *groups)
        self.image = image
        self.rect = self.image.get_rect()
        self.hitbox_rect = pygame.rect.Rect(self.rect)


class AnimatedSprite(Sprite):
    """
    Sprite that supports animation.

    Optional attributes:
    updates_per_frame -- number of updates to move to the following frame,
        also known as "animation speed".
    loop -- if true, loop animation indefinitely.
    animation_direction -- determines the order in which the images in the
        animation are used.
        EG: FORWARD (0, 1, 2), BACKWARD (2, 1, 0) or
        FORWARD_AND_BACKWARD (0, 1, 2, 1).
    """
    FORWARD, BACKWARD, FORWARD_AND_BACKWARD = range(3)

    def __init__(self, images, *groups):
        if not isinstance(images, list):
            images = [images]
        Sprite.__init__(self, images[0], *groups)
        self.images = images

        # optional attributes
        self.updates_per_frame = 1
        self.loop = True
        self.animation_direction = AnimatedSprite.FORWARD_AND_BACKWARD

        self._is_animation_done = False
        self._updates = 0
        self._current_frame = 0
        self._direction = AnimatedSprite.FORWARD

    def update(self):
        if not self._is_animation_done:
            self._update_frame_count()

    def _update_frame_count(self):
        if self._updates >= self.updates_per_frame:
            # for FORWARD and BACKWARD the modulo function always returns
            # the correct frame to render
            if self.animation_direction == AnimatedSprite.FORWARD or \
               self.animation_direction == AnimatedSprite.BACKWARD:
                if self.animation_direction == AnimatedSprite.FORWARD:
                    self._current_frame = (self._current_frame + 1) % len(self.images)
                elif self.animation_direction == AnimatedSprite.BACKWARD:
                    self._current_frame = (self._current_frame - 1) % len(self.images)
                if self._current_frame == 0 and not self.loop:
                    self._is_animation_done = True
                    return
            # for the FORWARD_AND_BACKWARD animation direction, a manual approach
            # is used instead, using _direction for control
            elif self.animation_direction == AnimatedSprite.FORWARD_AND_BACKWARD:
                if self._direction == AnimatedSprite.FORWARD:
                    self._current_frame += 1
                    if self._current_frame == len(self.images) - 1:
                        self._direction = AnimatedSprite.BACKWARD
                elif self._direction == AnimatedSprite.BACKWARD:
                    self._current_frame -= 1
                    if self._current_frame == 0:
                        self._direction = AnimatedSprite.FORWARD
                        if not self.loop:
                            self._is_animation_done = True
                            return
            self.image = self.images[self._current_frame]
            self._updates = 0
        self._updates += 1


class PlayerUnit(AnimatedSprite):
    NORMAL, FLASHING = range(2)

    def __init__(self, *groups):
        frames = self._load_animation_frames()
        AnimatedSprite.__init__(self, frames, *groups)

        self.updates_per_frame = 2
        self.loop = True
        self.state = self.NORMAL

    def update(self):
        AnimatedSprite.update(self)
        if self.state == self.FLASHING:
            self._flash()

    def _flash(self):
        self.image = self.image.copy()
        px_array = pygame.PixelArray(self.image)
        px_array.replace(WHITE, WHITE, 1.0)
        del px_array
        self.state = self.NORMAL

    def _load_animation_frames(self):
        image = resource_manager.load_image("player_unit.png")
        image.set_colorkey(TRANSPARENT_COLOR)
        image = image.convert_alpha()
        frames = resource_manager.slice_animated_sprite(image, (24, 24))
        return frames


class Bullet(Sprite):
    def __init__(self, entity, *groups):
        image = resource_manager.load_image("bullet.png")
        Sprite.__init__(self, image, *groups)
        self.entity = entity

    def update(self):
        self.rect.center = self.entity.pos


class EnemyBullet(Sprite):
    def __init__(self, entity, *groups):
        image = resource_manager.load_image("enemy_bullet.png")
        image.set_colorkey(TRANSPARENT_COLOR)
        Sprite.__init__(self, image, *groups)
        self.entity = entity

    def update(self):
        self.rect.center = self.entity.pos


class EnemyUnit(AnimatedSprite):
    NORMAL, FLASHING = range(2)

    def __init__(self, entity, *groups):
        frames = self._load_animation_frames()
        AnimatedSprite.__init__(self, frames, *groups)

        self.updates_per_frame = 2
        self.loop = True
        self.entity = entity
        self.state = self.NORMAL

    def update(self):
        AnimatedSprite.update(self)
        self.rect.center = self.entity.pos
        if self.state == self.FLASHING:
            self._flash()

    def _flash(self):
        self.image = self.image.copy()
        px_array = pygame.PixelArray(self.image)
        px_array.replace(WHITE, WHITE, 1.0)
        del px_array
        self.state = self.NORMAL

    def _load_animation_frames(self):
        image = resource_manager.load_image("enemy_unit.png")
        image.set_colorkey(TRANSPARENT_COLOR)
        image = image.convert_alpha()
        frames = resource_manager.slice_animated_sprite(image, (24, 24))
        return frames


class BalanceBar(Sprite):
    def __init__(self, *groups):
        image = resource_manager.load_image("balance_bar.png")
        image.set_colorkey(TRANSPARENT_COLOR)
        Sprite.__init__(self, image, *groups)


class BalanceBarBlue(AnimatedSprite):
    def __init__(self, entity, *groups):
        frames = self._load_animation_frames()
        AnimatedSprite.__init__(self, frames, *groups)

        self.updates_per_frame = 1
        self.loop = True
        self.entity = entity

    def update(self):
        AnimatedSprite.update(self)
        self.rect.center = self.entity.pos

    def _load_animation_frames(self):
        image = resource_manager.load_image("balance_blue.png")
        image.set_colorkey(TRANSPARENT_COLOR)
        frames = resource_manager.slice_animated_sprite(image, (58, 20))
        return frames

class BalanceBarOrange(AnimatedSprite):
    def __init__(self, entity, *groups):
        frames = self._load_animation_frames()
        AnimatedSprite.__init__(self, frames, *groups)

        self.updates_per_frame = 1
        self.loop = True
        self.entity = entity

    def update(self):
        AnimatedSprite.update(self)
        self.rect.center = self.entity.pos

    def _load_animation_frames(self):
        image = resource_manager.load_image("balance_orange.png")
        image.set_colorkey(TRANSPARENT_COLOR)
        frames = resource_manager.slice_animated_sprite(image, (58, 20))
        return frames
