import pygame

from event_manager import Listener
from models import Direction
import events


class Clock(Listener):
    def __init__(self, event_manager, max_fps):
        Listener.__init__(self, event_manager)
        self.em.register(self)

        self.max_fps = max_fps
        self.clock = pygame.time.Clock()
        self.keepGoing = True

    def notify(self, event):
        if isinstance(event, events.Quit):
            self.keepGoing = False

    def run(self):
        while self.keepGoing:
            self.em.post(events.Tick())
            self.clock.tick(self.max_fps)


class Keyboard(Listener):
    def __init__(self, event_manager):
        Listener.__init__(self, event_manager)
        self.em.register(self)

        self.directions = set()

    def update_directions(self, pg_event):
        if pg_event.type == pygame.KEYDOWN:
            if pg_event.key == pygame.K_LEFT:
                self.directions.add(Direction.LEFT)
            if pg_event.key == pygame.K_RIGHT:
                self.directions.add(Direction.RIGHT)
            if pg_event.key == pygame.K_UP:
                self.directions.add(Direction.UP)
            if pg_event.key == pygame.K_DOWN:
                self.directions.add(Direction.DOWN)

        if pg_event.type == pygame.KEYUP:
            if pg_event.key == pygame.K_LEFT:
                self.directions.discard(Direction.LEFT)
            if pg_event.key == pygame.K_RIGHT:
                self.directions.discard(Direction.RIGHT)
            if pg_event.key == pygame.K_UP:
                self.directions.discard(Direction.UP)
            if pg_event.key == pygame.K_DOWN:
                self.directions.discard(Direction.DOWN)

    def ignore_contradictory_directions(self, directions):
        dirs = set(directions)

        if Direction.UP in dirs and Direction.DOWN in dirs:
            dirs.discard(Direction.UP)
            dirs.discard(Direction.DOWN)

        if Direction.LEFT in dirs and Direction.RIGHT in dirs:
            dirs.discard(Direction.LEFT)
            dirs.discard(Direction.RIGHT)

        return dirs

    def notify(self, event):
        if isinstance(event, events.Tick):
            for pg_event in pygame.event.get():
                if (pg_event.type == pygame.KEYDOWN and \
                    pg_event.key == pygame.K_ESCAPE) or pg_event.type == pygame.QUIT:
                    self.em.post(events.Quit())
                    return
                self.update_directions(pg_event)

                if pg_event.type == pygame.KEYDOWN and pg_event.key == pygame.K_z:
                    self.em.post(events.PlayerUnitPrimaryFireRequest())

            dirs = self.ignore_contradictory_directions(self.directions)
            if len(dirs) > 0:
                self.em.post(events.PlayerUnitMoveRequest(dirs))

