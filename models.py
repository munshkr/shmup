from event_manager import Listener
from entity import Entity
import events
import random
import resource_manager
import bulletml


class Game(Listener):
    #INITIALIZED, RUNNING, PAUSED = range(3)

    DEBUG_MODE = False

    VIEWPORT_SIZE = (320, 240)
    VIEWPORT_WIDTH, VIEWPORT_HEIGHT = VIEWPORT_SIZE

    ENEMY_SPAWN_PERIOD = 13
    ENEMY_FIRE_PERIOD = 27

    def __init__(self, event_manager):
        Listener.__init__(self, event_manager)
        self.em.register(self)
        self.player = Player(event_manager, game = self)
        self.balance_bar = BalanceBar(event_manager,
                                      BalanceBar.WIDTH / 2,
                                      Game.VIEWPORT_HEIGHT - BalanceBar.HEIGHT / 2)

    def start(self):
        if Game.DEBUG_MODE:
            Game.ENEMY_FIRE_PERIOD = 0
            self._debug_first_run = False

        self.player.unit.x, self.player.unit.y = Player.INITIAL_SPAWN_POS
        self.player_bullets = set()
        self.enemies = set()
        self.enemy_bullets = set()
        self.ticks_until_next_spawn = Game.ENEMY_SPAWN_PERIOD
        self._enemy_spawn_counter = 0
        self.em.post(events.GameStart())

    def notify(self, event):
        if isinstance(event, events.Tick):
            self._clean_offscreen_entities(self.player_bullets)
            self._clean_offscreen_entities(self.enemies)
            self._clean_finished_bullets(self.enemy_bullets)
            self._update_entities(self.player_bullets)
            self._update_entities(self.enemies)
            self._update_enemy_bullets(self.enemy_bullets)
            self._handle_enemy_collisions()
            self._handle_player_collisions()
            self._spawn_enemies()
        elif isinstance(event, events.EnemyUnitPrimaryFire):
            if event.bullets:
                self.enemy_bullets.update(event.bullets)

    def _clean_offscreen_entities(self, entities):
        outsiders = set([e for e in entities if e.y < Game.VIEWPORT_HEIGHT - Game.VIEWPORT_HEIGHT * e.active_area_factor - e.height / 2 or
                                                e.x < Game.VIEWPORT_WIDTH - Game.VIEWPORT_WIDTH * e.active_area_factor - e.width / 2 or
                                                e.y > Game.VIEWPORT_HEIGHT + e.height / 2 or
                                                e.x > Game.VIEWPORT_WIDTH + e.width / 2])
        for o in outsiders:
            o.delete()
        entities.difference_update(outsiders)

    def _clean_finished_bullets(self, enemy_bullets):
        finished_bullets = set([b for b in enemy_bullets
                                if b.source.finished or (
                                b.y < Game.VIEWPORT_HEIGHT - Game.VIEWPORT_HEIGHT * b.active_area_factor - b.height / 2 or
                                b.x < Game.VIEWPORT_WIDTH - Game.VIEWPORT_WIDTH * b.active_area_factor - b.width / 2 or
                                b.y > Game.VIEWPORT_HEIGHT + b.height / 2 or
                                b.x > Game.VIEWPORT_WIDTH + b.width / 2)])
        for b in finished_bullets:
            b.delete()  # each enemy_bullet is an Entity
        enemy_bullets.difference_update(finished_bullets)

    def _update_entities(self, entities):
        for entity in entities:
            entity.update()

    def _update_enemy_bullets(self, enemy_bullets):
        if enemy_bullets:
            new_bullets = set()
            # make a copy of the set, since it changes size during the iteration
            bullets = [b for b in enemy_bullets]
            for b in bullets:
                new_bullets = b.update()
                if new_bullets:
                    self.enemy_bullets.update(new_bullets)
                    self.em.post(events.EnemyBulletFire(new_bullets))

    def _handle_enemy_collisions(self):
        deleted_enemies = set()
        for enemy in self.enemies:
            bullet_hits = enemy.collides_with(self.player_bullets)
            if bullet_hits:
                enemy.hit(bullet_hits)
                if enemy.health <= 0:
                    enemy.delete()
                    deleted_enemies.add(enemy)
            for bullet in bullet_hits:
                bullet.delete()
            self.player_bullets.difference_update(bullet_hits)
        self.enemies.difference_update(deleted_enemies)

    def _handle_player_collisions(self):
        bullet_hits = self.player.unit.collides_with(self.enemy_bullets)
        if bullet_hits:
            self.player.unit.hit(bullet_hits)
        for bullet in bullet_hits:
            bullet.delete()
        self.enemy_bullets.difference_update(bullet_hits)

    def _spawn_enemies(self):
        if not Game.DEBUG_MODE:
            self._enemy_spawn_counter += 1
            if self._enemy_spawn_counter >= self.ticks_until_next_spawn:
                self._enemy_spawn_counter = 0
                enemy_unit = EnemyUnit(self.em,
                                       x = random.randint(0, Game.VIEWPORT_WIDTH),
                                       y = 0,
                                       speed = random.randint(1, 3),
                                       health = 2,
                                       game = self)
                enemy_unit.y = -enemy_unit.height / 2
                self.enemies.add(enemy_unit)
                enemy_unit.spawn()
        else:
            if not self._debug_first_run:
                self._debug_first_run = True
                enemy_unit = EnemyUnit(self.em,
                                       x = 0,
                                       y = 0,
                                       speed = 0,
                                       health = 1000,
                                       game = self)
                enemy_unit.pos = (Game.VIEWPORT_WIDTH / 2, Game.VIEWPORT_WIDTH / 5)
                self.enemies.add(enemy_unit)
                enemy_unit.spawn()


class Player(object):
    if not Game.DEBUG_MODE:
        INITIAL_SPAWN_POS = (Game.VIEWPORT_WIDTH / 2, Game.VIEWPORT_HEIGHT - 15)
    else:
        INITIAL_SPAWN_POS = (Game.VIEWPORT_WIDTH / 2, Game.VIEWPORT_HEIGHT / 2)

    def __init__(self, event_manager, game):
        self.game = game
        self.unit = PlayerUnit(event_manager, player = self)


class Direction(object):
    LEFT, UP, RIGHT, DOWN = range(4)


class PlayerUnit(Listener, Entity):
    DEFAULT_SPEED = 3

    def __init__(self, event_manager, player):
        Entity.__init__(self)
        self.size = (24, 24)
        self.hitbox_size = (8, 16)

        Listener.__init__(self, event_manager)
        self.em.register(self)

        self.player = player
        self.speed = PlayerUnit.DEFAULT_SPEED

    def spawn(self):
        self.em.post(events.PlayerUnitSpawn(self))

    def can_move(self, distance, directions):
        # if a single direction is passed, convert it to a list
        if isinstance(directions, int):
            directions = [directions]
        # FIXME Use a margin
        for direction in directions:
            if direction == Direction.LEFT and self.x - distance < 0:
                return False
            if direction == Direction.UP and self.y - distance < 0:
                return False
            if direction == Direction.RIGHT and self.x + distance > Game.VIEWPORT_WIDTH:
                return False
            if direction == Direction.DOWN and self.y + distance > Game.VIEWPORT_HEIGHT:
                return False
        return True

    def move_distance(self, dirs):
        distance = self.speed

        # Move slowly when going to diagonals, except when touching a border
        if (Direction.UP in dirs or Direction.DOWN in dirs) and \
           (Direction.LEFT in dirs or Direction.RIGHT in dirs) and \
           self.can_move(distance, range(4)):
            # sqrt(2) / 2 =~ 0.707
            distance = distance * 0.707

        return distance

    def move(self, distance, dirs):
        if Direction.LEFT in dirs:
            self.x -= distance
        if Direction.RIGHT in dirs:
            self.x += distance
        if Direction.UP in dirs:
            self.y -= distance
        if Direction.DOWN in dirs:
            self.y += distance

    def hit(self, bullets):
        #self.health -= sum([b.power for b in bullets])
        self.em.post(events.PlayerUnitHit(self))

    def primary_fire(self):
        left  = PlayerBullet(x = self.x - 4, y = self.y)
        right = PlayerBullet(x = self.x + 3, y = self.y)
        bullets = set([left, right])
        self.player.game.player_bullets.update(bullets)
        return bullets

    def notify(self, event):
        if isinstance(event, events.GameStart):
            self.spawn()
        elif isinstance(event, events.PlayerUnitMoveRequest):
            dist = self.move_distance(event.directions)
            event.directions = set([d for d in event.directions if self.can_move(dist, d)])
            if len(event.directions) > 0:
                self.move(dist, event.directions)
                self.em.post(events.PlayerUnitMove(self))
        elif isinstance(event, events.PlayerUnitPrimaryFireRequest):
            bullets = self.primary_fire()
            self.em.post(events.PlayerUnitPrimaryFire(bullets))


class Bullet(Entity):
    def __init__(self, x, y):
        Entity.__init__(self, x, y)


class PlayerBullet(Bullet):
    def __init__(self, x, y):
        Bullet.__init__(self, x, y)
        self.size = (1, 8)
        self.speed = 10
        self.power = 1

    def update(self):
        distance = self.speed
        self.y -= distance


class EnemyBullet(Bullet):
    def __init__(self, enemy_unit, x, y, is_source):
        Bullet.__init__(self, x, y)
        self.enemy_unit = enemy_unit
        self.power = 1
        self.size = (3, 3)
        if is_source:
            self.source = self._load_source(target = self.enemy_unit.game.player.unit)

    def update(self):
        new_bullets = set()
        new_sources = self.source.step()
        self.pos = self.source.x, self.source.y
        for s in new_sources:
            b = EnemyBullet(self.enemy_unit, s.x, s.y, is_source = False)
            b.source = s
            new_bullets.add(b)
        return new_bullets

    def _load_source(self, target):
        xml_file = "test.xml"
        if Game.DEBUG_MODE:
            xml_file = "debug.xml"

        doc = resource_manager.load_bulletml(xml_file)
        return bulletml.Bullet.FromDocument(doc,
                                            x = self.x,
                                            y = self.y,
                                            target = target,
                                            rank = 1,
                                            speed = 2,
                                            direction = 0)


class EnemyUnit(Listener, Entity):
    def __init__(self, event_manager, game, x, y, speed, health):
        Listener.__init__(self, event_manager)
        self.em.register(self)

        Entity.__init__(self, x, y)
        self.size = (24, 24)
        self.hitbox_size = (16, 16)

        self.game = game
        self.speed = speed
        self.health = health
        self.ticks_until_next_shot = Game.ENEMY_FIRE_PERIOD
        self._tick_count = 0
        self._debug_first_run = False

    def spawn(self):
        self.em.post(events.EnemyUnitSpawn(self))

    def update(self):
        distance = self.speed
        self.y += distance

    def hit(self, bullets):
        self.health -= sum([b.power for b in bullets])
        self.em.post(events.EnemyUnitHit(self))

    def primary_fire(self):
        if not Game.DEBUG_MODE:
            origin = EnemyBullet(enemy_unit = self, x = self.x, y = self.y, is_source = True)
            origin.source.vanished = True
            bullets = set([origin])
            return bullets
        else:
            if not self._debug_first_run:
                self._debug_first_run = True
                origin = EnemyBullet(enemy_unit = self, x = self.x, y = self.y, is_source = True)
                origin.source.vanished = True
                bullets = set([origin])
                return bullets


    def notify(self, event):
        if isinstance(event, events.Tick):
            self._tick_count += 1
            if self._tick_count >= self.ticks_until_next_shot:
                self._tick_count = 0
                bullets = self.primary_fire()
                self.em.post(events.EnemyUnitPrimaryFire(bullets))

    def delete(self):
        Entity.delete(self)
        self.em.unregister(self)


class HudItem(object):
    def __init__(self, x = 0, y = 0):
        self.x, self.y = (x, y)

    @property
    def pos(self):
        return (self.x, self.y)

    @pos.setter
    def pos(self, value):
        self.x, self.y = value


class BalanceBar(Listener, HudItem):
    WIDTH, HEIGHT = (128, 26)

    def __init__(self, event_manager, x = 0, y = 0):
        Listener.__init__(self, event_manager)
        self.em.register(self)

        HudItem.__init__(self, x, y)

    def notify(self, event):
        if isinstance(event, events.GameStart):
            self.spawn()

    def spawn(self):
        self.em.post(events.BalanceBarInit(self))
