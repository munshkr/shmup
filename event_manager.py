from weakref import WeakKeyDictionary
import time
import events


class Listener(object):
    def __init__(self, event_manager):
        self.em = event_manager

    def notify(self, event):
        raise NotImplementedError


class EventManager(object):
    def __init__(self):
        self.listeners = {} #WeakKeyDictionary()

    def register(self, listener):
        self.listeners[listener] = True

    def unregister(self, listener):
        if listener in self.listeners.keys():
            del self.listeners[listener]

    def post(self, event):
        #if not isinstance(event, events.Tick):
           #print "%.3f: %s" % (time.time(), event)

        for listener in self.listeners.keys():
            listener.notify(event)



class SelectiveEventManager(object):
    def __init__(self):
        self.listeners = {}

    def register(self, event, listener):
        if not self.listeners.has_key(event):
            self.listeners[event] = []
        self.listeners[event].append(listener)

    def post(self, event):
        if self.listeners.has_key(event):
            for listener in self.listeners[event]:
                listener.notify(event)
