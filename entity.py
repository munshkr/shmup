import pygame

class Entity(object):
    ACTIVE, DELETED = range(2)

    def __init__(self, x = 0, y = 0):
        self.hitbox_rect = pygame.Rect(0, 0, 0, 0)
        self.hitbox_rect.centerx, self.hitbox_rect.centery = (x, y)
        self.active_area_factor = 1
        self.state = Entity.ACTIVE

    def __repr__(self):
        return '<%s %s: pos=(%i, %i)>' % (self.__class__.__name__, id(self), self.x, self.y)

    @property
    def pos(self):
        return (self.hitbox_rect.centerx, self.hitbox_rect.centery)
    @pos.setter
    def pos(self, value):
        self.hitbox_rect.centerx, self.hitbox_rect.centery = value

    @property
    def x(self):
        return self.hitbox_rect.centerx
    @x.setter
    def x(self, value):
        self.hitbox_rect.centerx = value

    @property
    def y(self):
        return self.hitbox_rect.centery
    @y.setter
    def y(self, value):
        self.hitbox_rect.centery = value

    @property
    def width(self):
        return self.hitbox_rect.width
    @width.setter
    def width(self, value):
        self.hitbox_rect.width = value

    @property
    def height(self):
        return self.hitbox_rect.height
    @height.setter
    def height(self, value):
        self.hitbox_rect.height = value

    @property
    def hitbox_size(self):
        return (self.hitbox_rect.width, self.hitbox_rect.height)
    @hitbox_size.setter
    def hitbox_size(self, value):
        self.hitbox_rect.width, self.hitbox_rect.height = value

    def delete(self):
        self.state = Entity.DELETED

    def update(self):
        raise NotImplementedError

    def collides_with(self, entity_or_entities):
        if isinstance(entity_or_entities, Entity):
            if self.hitbox_rect.colliderect(entity_or_entities.hitbox_rect):
                return entity_or_entities
            return None
        return set([e for e in entity_or_entities if self.hitbox_rect.colliderect(e.hitbox_rect)])

